# Description

%{first_multiline_commit}

# Checklist

Thanks for your contribution! Please complete and check off the following quality control tasks.

Changes:

- [ ] I created this merge request from my own fork and from a branch other than main or master.
- [ ] I checked that the CI pipeline completes without errors.
- [ ] I added a change log entry (or this is a minor change that does not require a change log entry).

Whitespace:

- [ ] No line that I added [ends with trailing whitespace](https://stackoverflow.com/questions/21410075/what-is-trailing-whitespace-and-how-can-i-handle-this).
- [ ] Every file that I modified [ends with one blank line](https://unix.stackexchange.com/questions/18743/whats-the-point-in-adding-a-new-line-to-the-end-of-a-file).

Grammar and spelling:

- [ ] I proofread my contribution and made sure that it is free from spelling and grammatical errors.
- [ ] I used [American English rather than British English](https://en.wikipedia.org/wiki/American_and_British_English_spelling_differences) spelling.
- [ ] I used the [Oxford (serial) comma](https://style.mla.org/serial-commas-and-semicolons/) in lists of three or more items.
- [ ] I minimized use of the [passive voice](https://owl.purdue.edu/owl/general_writing/academic_writing/active_and_passive_voice/more_about_passive_voice.html#:~:text=In%20a%20sentence%20using%20passive,it%20uses%20the%20passive%20voice.).

Figures and code:

- [ ] I contributed [plot directive code](https://matplotlib.org/stable/api/sphinxext_plot_directive_api.html) to generate figures (or I did not contribute any figures).
- [ ] I contributed Python code that conforms to the [PEP 8 style guide](https://peps.python.org/pep-0008/) (or I did not contribute Python code).
